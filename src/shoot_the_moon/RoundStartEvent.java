package shoot_the_moon;


public class RoundStartEvent extends Event {
	Position dealer;
	
	public RoundStartEvent( Position dealer ) {
		this.dealer = dealer;
	}
	
	public Position getDealer() {
		return dealer;
	}

}
