package shoot_the_moon;


import java.util.Collections;
import java.util.List;


public abstract class Player implements Observer {

	private List<Card> hand;

	private List<Card> readOnlyHand;

	private Trump currentTrump;

	private Bid lastHighestBid;

	private Card leadCard;

	private Position position;
	
	private GameSettings settings;

	/**
	 * Called when the player is dealt a card 
	 * @param c the card dealt
	 */
	public final void acceptCard( Card c ) {
		//if our hand hasn't been initialized, first create it
		if ( hand == null ) {
			hand = ListFactory.makeList();
			readOnlyHand = Collections.unmodifiableList( hand );
		}
		
		//add the card
		hand.add( c );
	}

	/**
	 * Called to tell a Player what his position is at the table. Should never
	 * be called by a subclass of Player.
	 * 
	 * @param position :
	 *            the player's assigned position
	 */
	public void acceptPosition( Position position ) {
		this.position = position;
	}

	/**
	 * Called to make a Player make his/her bid. Should never be called by a
	 * subclass of Player.
	 * 
	 * @return the Bid that this player is making
	 */
	public final Bid makeBid() {
		Bid bid = decideBid( lastHighestBid );
		
		return bid;
	}

	/**
	 * Called by Game event to send events to the Player. Should never be called
	 * by a subclass of Player.
	 * 
	 * @param e :
	 *            An Event sent from the Game
	 */
	public final void observeEvent( Event e ) {
		if ( e instanceof GameStartEvent ) {
			GameStartEvent ge = (GameStartEvent) e;
			settings = ge.getSettings();
		}
		if ( e instanceof ContractMadeEvent ) {
			ContractMadeEvent cme = (ContractMadeEvent) e;
			currentTrump = cme.getWinningBid().getTrump();
		} else if ( e instanceof BidMadeEvent ) {
			BidMadeEvent bne = (BidMadeEvent) e;
			if ( lastHighestBid != null ) {
				if ( bne.getBid().isBetterThan( lastHighestBid ) ) {
					lastHighestBid = bne.getBid();
				}
			}
			else {
				lastHighestBid = bne.getBid();
			}
		} else if ( e instanceof RoundStartEvent ) {
			lastHighestBid = null;
			currentTrump = null;
			leadCard = null;
			hand = null;
		} else if ( e instanceof TrickWonEvent ) {
			leadCard = null;
		} else if ( e instanceof CardPlayedEvent ) {
			if ( leadCard == null ) {
				CardPlayedEvent cpe = (CardPlayedEvent) e;
				leadCard = cpe.getCard();
			}
		}
		processEvent( e );
	}

	/**
	 * Called by a Game to make this Player play a card This function is not to
	 * be called by a subclass.
	 * 
	 * @return the Card this player is playing
	 */
	public final Card playCard() {
		List<Card> legalCards = Rules.getLegalCards( hand, leadCard,
				currentTrump );
		Card card = decideCard( legalCards );
		if ( hand.contains( card ) ) {
			hand.remove( card );
			return card;
		} else {
			throw new RuntimeException( "Player tried to play illegal card." );
		}
	}

	/**
	 * Called to make a Player decide a bid
	 * 
	 * @param lastHighestBid :
	 *            the lastHighestBid observed by the Player this round (may be
	 *            null)
	 * @returns Bid b where b.getNumber() == Bid.PASS or b.isGreaterThan(
	 *          lastHighestBid )
	 */
	protected abstract Bid decideBid( Bid lastHighestBid );

	/**
	 * Called to make a Player decide on a Card to play
	 * 
	 * @param legalCards :
	 *            a list of the legal cards to play based on the current
	 *            observed state of the round
	 * @returns Card c such that legalCards.contains( c ) == true;
	 */
	protected abstract Card decideCard( List<Card> legalCards );

	/**
	 * Called whenever a Player observes an event happening in a Game
	 * 
	 * @param e :
	 *            the observed Event
	 */
	protected abstract void processEvent( Event e );
	
	/**
	 * Gets the player's name
	 * @return the player's name
	 */
	public abstract String getName();

	/**
	 * Returns what the trump is currently, according to the messages sent to
	 * this Player. Returns null if no trump has been named.
	 */
	protected final Trump getCurrentTrump() {
		return currentTrump;
	}

	/**
	 * Function to get the read-only view of a player's hand. Returns null if no
	 * hand has been assigned yet.
	 * 
	 * @returns read-only view of this player's hand (List<Card>)
	 */
	protected final List<Card> getHand() {
		return readOnlyHand;
	}

	/**
	 * Based on Events passed to the Player, returns the last highest bid made
	 * this round.
	 * 
	 * @return null if no bids have been made yet this round, the highest bid
	 *         observed this round otherwise
	 */
	protected final Bid getLastHighestBid() {
		return lastHighestBid;
	}

	/**
	 * Based on events passed to the Player, returns the lead card in a trick.
	 * Returns null if no lead card has been played in the current trick.
	 */
	protected final Card getLeadCard() {
		return leadCard;
	}

	/**
	 * @return the PlayerPosition assigned to this player
	 */
	protected final Position getPosition() {
		return position;
	}
	
	/**
	 * @return the settings passed to this player by the game
	 */
	protected final GameSettings getSettings() {
		return settings;
	}

}
