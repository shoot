package shoot_the_moon;


import java.util.Map;


public class RoundOverEvent extends Event {
	private boolean contractMade;
	private Bid contract;
	private Position contractMaker;
	private Map<Team, Integer> score;
	private Map<Team, Integer> tricksTaken;

	public RoundOverEvent( boolean contractMade, Map<Team, Integer> score, Bid contract, Map<Team, Integer> tricksTaken, Position contractMaker ) {
		this.contractMade = contractMade;
		this.score = score;
		this.contract = contract;
		this.tricksTaken = tricksTaken;
		this.contractMaker = contractMaker;
	}

	public int getScore( Team team ) {
		return score.get( team ).intValue();
	}
	
	public int getTricksTaken( Team team ) {
		return tricksTaken.get( team ).intValue();
	}
	
	public boolean isContractMade() {
		return contractMade;
	}
	
	public Bid getContract() {
		return contract;
	}
	
	public Position getContractMaker() {
		return contractMaker;
	}

}
