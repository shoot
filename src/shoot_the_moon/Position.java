package shoot_the_moon;


public class Position {
	int position;
	int numPositions;
	Team team;
	String name;
	
	public Position( int position, int numPositions, Team team, String name ) {
		this.position = position;
		this.numPositions = numPositions;
		this.team = team;
		this.name = new String( name );
	}
	
	public String getName() {
		return name;
	}
	
	public int getPosition() {
		return position;
	}
	
	public Team getTeam() {
		return team;
	}
	
	public boolean isOnTeamWith( Position p ) {
		return p.getTeam() == this.getTeam();
	}
	
	public int getDistanceFrom( Position p ) {
		return ( this.getPosition() - p.getPosition() ) % numPositions;
	}
	
	public String toString() {
		return name + " (position " + position + ")";
	}
}
