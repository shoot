package shoot_the_moon;


public interface Observer {
	public void observeEvent( Event e );
}
