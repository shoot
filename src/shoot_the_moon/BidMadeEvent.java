package shoot_the_moon;


public class BidMadeEvent extends Event {
	private Bid bid;

	private Position position;

	public BidMadeEvent( Bid bid, Position position ) {
		this.bid = bid;
		this.position = position;
	}

	/**
	 * @return the bid
	 */
	public Bid getBid() {
		return bid;
	}

	/**
	 * @return the position
	 */
	public Position getPosition() {
		return position;
	}

}
