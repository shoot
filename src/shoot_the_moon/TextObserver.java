package shoot_the_moon;


import java.io.PrintStream;

public class TextObserver implements Observer {
	PrintStream ps;

	public TextObserver( PrintStream ps ) {
		this.ps = ps;
		ps.print( "**********************\n" 
				+ "*   SHOOT THE MOON   *\n"
				+ "**********************\n"
                + " v." + ShootTheMoon.VERSION + "\n\n");
	}

	public void observeEvent( Event e ) {
		if ( e instanceof GameStartEvent ) {
			GameStartEvent gse = (GameStartEvent) e;
			ps.print( "The game is now starting. The settings are:\n" +
					"\t" + gse.getSettings().getNumPlayersPerTeam() + "\t players per team.\n" +
					"\t" + gse.getSettings().getNumDuplicateCards() + "\t copies of each card in the deck" +
							"\n\n" );
		} else if ( e instanceof RoundStartEvent ) {
			RoundStartEvent rse = (RoundStartEvent) e;
			ps.print( "\t" + getPositionString( rse.getDealer() )  + " deals the cards.\n" );
		} else if ( e instanceof BidMadeEvent ) {
			BidMadeEvent bme = (BidMadeEvent) e;
			ps.print( "\t\t" + getPositionString( bme.getPosition() ) + " bids " + bme.getBid().toString() + "\n");
		} else if ( e instanceof ContractMadeEvent ) {
			ContractMadeEvent cme = (ContractMadeEvent) e;
			ps.print( "\t" + getPositionString( cme.getWinningPosition() ) + " wins the contract at " + cme.getWinningBid().toString() +"\n\n");
		} else if ( e instanceof CardPlayedEvent ) {
			CardPlayedEvent cpe = (CardPlayedEvent) e;
			ps.print( "\t\t\t" + getPositionString( cpe.getPosition() ) + " plays the " + cpe.getCard().getShortString() +"\n" );
		} else if ( e instanceof TrickWonEvent ) {
			TrickWonEvent twe = (TrickWonEvent) e;
			ps.print( "\t\t" + getPositionString( twe.getWinningPlayer() ) + " won the trick.\n\n" );
		} else if ( e instanceof RoundOverEvent ) {
			RoundOverEvent roe = (RoundOverEvent) e;
			ps.print( "The tricks taken this round were: \n");
			for ( Team t: Team.allTeams ) {
				ps.print( "\t" + t.getName() + "\t: " + roe.getTricksTaken( t ) + " tricks\n" );
			}
			ps.print( "The contract of " + roe.getContract() + " made by " + getPositionString( roe.getContractMaker() ) + " was " +(roe.isContractMade() ? "" : "not " ) + "made. The score is now...\n" );
			for ( Team t : Team.allTeams ) {
				ps.print( t.getName() + "\t: " + roe.getScore( t ) + " points\n" );
			}
			ps.print( '\n' );
		} else if ( e instanceof GameOverEvent ) {
			GameOverEvent goe = (GameOverEvent) e;
			ps.print( "The game is over. ");
			if ( goe.isATie() ) {
				ps.print( "The game ended in a tie.\n" );
			} else {
				ps.print( goe.getWinner().getName() + " won the game.\n" );
			}
		}
	}
	
	private String getPositionString( Position p ) {
		return p.getName() + " (" + p.getTeam().getName() + ")";
	}
}
