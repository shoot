package shoot_the_moon;


/*
 * Represents the states a Euchre card's rank can be:
 * 9, 10, J, Q, K, A
 * This can & should be treated like an enum:
 * ie. Rank r = Rank.NINE
 */
public final class Rank {

	private int ranking;

	private String shortName;

	private String fullName;

	private ContextualRank cRank;

	private Rank( int ranking, String shortName, String fullName,
			ContextualRank cRank ) {
		this.ranking = ranking;
		this.shortName = shortName;
		this.fullName = fullName;
		this.cRank = cRank;
	}

	static final Rank NINE = new Rank( 9, "9", "Nine", ContextualRank.NINE );

	static final Rank TEN = new Rank( 10, "10", "Ten", ContextualRank.TEN );

	static final Rank JACK = new Rank( 11, "J", "Jack", ContextualRank.JACK );

	static final Rank QUEEN = new Rank( 12, "Q", "Queen", ContextualRank.QUEEN );

	static final Rank KING = new Rank( 13, "K", "King", ContextualRank.KING );

	static final Rank ACE = new Rank( 14, "A", "Ace", ContextualRank.ACE );

	/*
	 * allRanks[] is an array of all the ranks which can be useful if you need
	 * to iterate over every possible rank
	 */
	static final Rank[] allRanks = { NINE, TEN, JACK, QUEEN, KING, ACE };

	/**
	 * @return the full name of the rank ie. Queen
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @return the integer ranking of the card ie. JACK.getRanking() = 11
	 */
	public int getRanking() {
		return ranking;
	}

	/**
	 * @return the short-form name for the rank ie. J, Q, K, etc.
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @return the corresponding ContextualRank for this rank
	 */
	public ContextualRank getContextualRank() {
		return cRank;
	}

}
