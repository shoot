package shoot_the_moon;

/*
 * Represents the states a Euchre card's contextual rank can be:
 * 9, 10, J, Q, K, A, Left, Right
 * This can & should be treated like an enum:
 * ie. ContextualRank r = ContextualRank.RIGHT
 */
public final class ContextualRank {

	private int ranking;

	private String name;

	private ContextualRank( int ranking, String name ) {
		this.ranking = ranking;
		this.name = name;
	}

	/*
	 * The following define the only states a ContextualRank reference can be
	 * since the constructor is declared private and the class is declared final
	 */
	static final ContextualRank NINE = new ContextualRank( 9, "Nine" );

	static final ContextualRank TEN = new ContextualRank( 10, "Ten" );

	static final ContextualRank JACK = new ContextualRank( 11, "Jack" );

	static final ContextualRank QUEEN = new ContextualRank( 12, "Queen" );

	static final ContextualRank KING = new ContextualRank( 13, "King" );

	static final ContextualRank ACE = new ContextualRank( 14, "Ace" );

	static final ContextualRank LEFT = new ContextualRank( 15, "Left" );

	static final ContextualRank RIGHT = new ContextualRank( 16, "Right" );

	/*
	 * allContextualRanks[] is an array of all the contextual ranks which can be
	 * useful if you need to iterate over every possible rank
	 */
	static final ContextualRank[] allContextualRanks = { NINE, TEN, JACK,
			QUEEN, KING, ACE, LEFT, RIGHT };

	/**
	 * @return the full name of the rank ie. Queen
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the integer ranking of the card ie. JACK.getRanking() = 11
	 */
	public int getRanking() {
		return ranking;
	}

	public String toString() {
		return name;
	}

}
