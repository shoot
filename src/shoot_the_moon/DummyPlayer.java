package shoot_the_moon;


import java.util.List;


/**
 * Dummy AI class that will always bid one higher than the previous bid in
 * hearts (unless the maximum bid is reached then it will pass) and will always
 * play it's first legal card in it's hand.
 * 
 * @author ben
 * 
 */
public class DummyPlayer extends Player {
	private String name;
	
	public DummyPlayer( String name ) {
		this.name = name;
	}
	@Override
	protected Bid decideBid( Bid lastHighestBid ) {
		if ( lastHighestBid == null ) {
			return Bid.makeNormalBid( 1, Trump.HEARTS );
		}
		if ( lastHighestBid.isPass() ) {
			return Bid.makeNormalBid( 1, Trump.HEARTS );
		} else if ( lastHighestBid.isNormalBid() ) {
			int lastBid = lastHighestBid.getNumber();
			if ( lastBid >= Rules.getHandSize( this.getSettings() ) ) {
				return Bid.makePassBid();
			} else {
				return Bid.makeNormalBid( lastBid +1, Trump.HEARTS );
			}
		//else the last highest bid was a shoot 
		} else {
			return Bid.makePassBid();
		}
	}

	@Override
	protected Card decideCard( List<Card> legalCards ) {
//		System.out.print( this.getPosition() + " has the hand: " );
//		List<Card> hand = ListFactory.makeList( this.getHand() );
//		Collections.sort( hand, new CardComparator( this.getCurrentTrump() ) );
//		
//		for ( Card c: hand ) {
//			System.out.print( c.toString() + " ");
//		}
//		System.out.print( '\n' );
		
		return legalCards.get( 0 );
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	protected void processEvent( Event e ) {
		//do nothing
	}

}
