package shoot_the_moon;


public class CardDealtEvent extends Event {
	private Position p;
	
	public CardDealtEvent( Position p ) {
		this.p = p;
	}
	
	public Position getPosition() {
		return p;
	}
	
}
