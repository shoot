package shoot_the_moon;


/*
 * Represents the 6 states that trump can be
 * This can & should be treated like an enum:
 * ie. Trump t = Trump.HIGH;
 */
public final class Trump {
	private String name;

	private boolean isSuit;

	private Suit suit;

	private Trump( String name, boolean isSuit, Suit suit ) {
		this.name = name;
		this.isSuit = isSuit;
		this.suit = suit;
	}

	public static final Trump SPADES = new Trump( "Spades", true, Suit.SPADES );

	public static final Trump HEARTS = new Trump( "Hearts", true, Suit.HEARTS );

	public static final Trump DIAMONDS = new Trump( "Diamonds", true,
			Suit.DIAMONDS );

	public static final Trump CLUBS = new Trump( "Clubs", true, Suit.CLUBS );

	public static final Trump HIGH = new Trump( "High", false, null );

	public static final Trump LOW = new Trump( "Low", false, null );

	/**
	 * @return if the trump is a suit or not
	 */
	public boolean isSuit() {
		return isSuit;
	}

	/**
	 * @return the name of trump ie. Clubs, High, Low
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the Suit that this trump represents, or null if isSuit() is false
	 */
	public Suit getSuit() {
		return suit;
	}

}