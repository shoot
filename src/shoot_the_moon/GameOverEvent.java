package shoot_the_moon;


import java.util.List;


public class GameOverEvent extends Event {
	Team winner;
	private boolean tied;
	private List<Team> tiedWinners;
	
	public GameOverEvent( Team winner ) {
		this.winner = winner;
		tied = false;
	}
	
	public GameOverEvent( List<Team> tiedWinners ) {
		tied = true;
		this.tiedWinners = tiedWinners;
	}
	
	public Team getWinner() {
		if ( tied ) throw new RuntimeException( "Illegal call to GameOverEvent.getWinner(), game was tied." );
		return winner;
	}
	
	public List<Team> getTiedWinners() {
		if ( !tied ) throw new RuntimeException( "Illegal call to GameOverEvent.getTiedWiners(), game was not tied." );
		return ListFactory.makeList( tiedWinners );
	}
	
	public boolean isATie() {
		return tied;
	}

}
