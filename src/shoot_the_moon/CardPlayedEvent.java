package shoot_the_moon;


public class CardPlayedEvent extends Event {
	private Position position;
	private Card card;
	
	public CardPlayedEvent( Position position, Card card ) {
		this.position = position;
		this.card = card;
	}

	/**
	 * @return the card
	 */
	public Card getCard() {
		return card;
	}
 
	/**
	 * @return the position
	
	public String toString() {
		return "Card dealt to " + p.toString();
	}
	 */
	public Position getPosition() {
		return position;
	}

}
