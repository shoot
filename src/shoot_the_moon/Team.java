package shoot_the_moon;

public final class Team implements Comparable<Team> {
	
	private Team() {
		
	}
	
	public static final Team TEAM1 = new Team();
	public static final Team TEAM2 = new Team();
	
	public static final Team[] allTeams = { TEAM1, TEAM2 };
	public static final int numTeams = allTeams.length;

	public int compareTo( Team t ) {
		if ( t == this ) return 0;
		if ( t == TEAM1 ) return 1;
		else return -1;
	}
	
	public String getName() {
		if ( this == TEAM1 ) { 
			return "Good guys";
		} else {
			return "Bad guys";
		}
	}
	
}
