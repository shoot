package shoot_the_moon;


public final class Bid {
	private int number;
	private Trump trump;
	
	private boolean isShoot;
	private boolean isPass;
	private int shootNumber;
	
	private Bid( int number, Trump trump, boolean isShoot, boolean isPass, int shootNumber ) {
		this.number = number;
		this.trump = trump;
		this.isShoot = isShoot;
		this.isPass = isPass;
		this.shootNumber = shootNumber;
	}
	
	/**
	 * Makes a bid that is neither a pass or a shoot (ie. a normal bid)
	 * @param number The number of the bid (ie. "6 hearts"-> number = 6)
	 * @param trump The trump suit associated with the bid
	 * @return a new normal Bid with given number and trump
	 */
	public final static Bid makeNormalBid( int number, Trump trump ) {
		return new Bid( number, trump, false, false, -1 );
	}
	
	/**
	 * Makes a bid that is a pass 
	 * @return a new Bid that represents a pass
	 */
	public final static Bid makePassBid( ) {
		return new Bid( -1, null, false, true, -1 );
	}
	
	/**
	 * Makes a shoot bid
	 * @param shootNumber Single shoot = 1, Double shoot = 2, etc...
	 * @param trump trump suit to shoot in
	 * @return a new Shoot bid with given shoot number and trump
	 */
	public final static Bid makeShootBid( int shootNumber, Trump trump ) {
		return new Bid( -1, trump, true, false, shootNumber );
	}

	/**
	 * @return true if this bid is a pass, false otherwise
	 */
	public boolean isPass() {
		return isPass;
	}

	/**
	 * @return true if this bid is a shoot, false otherwise
	 */
	public boolean isShoot() {
		return isShoot;
	}
	
	/**
	 * @return true if this bid is neither a shoot or a pass, false otherwise
	 */
	public boolean isNormalBid() {
		return ( !isPass && !isShoot );
	}

	/**
	 * Gets the number associated with a normal bid.  This method will throw a RuntimeException if the bid is not a normal bid (ie shoot or pass)
	 * @return the number associated with the bid (ie. "I bid 5 hearts"-> number = 5)
	 */
	public int getNumber() {
		if ( isShoot || isPass ) {
			throw new RuntimeException( "Illegal call to getNumber(), bid is a shoot or pass so it does not have a number.");
		}
		return number;
	}

	/**
	 * If the bid was a shoot, returns what kind of shoot it was.
	 * A single shoot will return 1, a double shoot will return 2, etc...
	 * Throws a runtime exception if the bid is not a shoot.
	 * @return the shoot number
	 */
	public int getShootNumber() {
		if ( !isShoot() ) {
			throw new RuntimeException( "Illegal call to getShootNumber(), bid is not a shoot." );
		}
		return shootNumber;
	}

	/**
	 * Gets the trump associated with this bid.  Throws a runtime exception if the bid was a pass. 
	 * @return trump associated with bid
	 */
	public Trump getTrump() {
		if ( isPass ) {
			throw new RuntimeException( "Illegal call to getTrump(), bid is a pass and has no trump" );
		}
		return trump;
	}
	
	/**
	 * @param otherBid the other bid to check against
	 * @return whether or not this bid is greater than another bid (ie, would beat another bid for the highest bid)
	 */
	public boolean isBetterThan( Bid otherBid ) {
		if ( otherBid == null ) throw new RuntimeException( "Illegal null argument passed to Bid.isBetterThan()." );
		//if other bid is a pass and this isn't, return true
		if ( otherBid.isPass() & !this.isPass() ) return true;
		//if this a shoot, but the other isn't, return true
		if ( !otherBid.isShoot() & this.isShoot() ) return true;
		//if both bids are passes, return false
		if ( otherBid.isPass() && this.isPass() ) return false;
		//if both bids are regular bids, check number
		if ( otherBid.isNormalBid() && this.isNormalBid() ) {
			return this.getNumber() > otherBid.getNumber();
		}
		//if both bids are shoots, check shoot number
		if ( otherBid.isShoot() && this.isShoot() ) {
			return this.getShootNumber() > otherBid.getShootNumber();
		}
		//all other cases, other bid is better, so return false
		return false;
	}
	
	public String toString() {
		if (isPass()) {
			return "pass";
		} else if ( isShoot() ) {
			return "shoot(x" + shootNumber+") in " + trump.getName();
		} else {
			return number + " " + trump.getName();
		}
	}
	
	
}
