package shoot_the_moon;


public class GameStartEvent extends Event {
	private GameSettings settings;

	public GameStartEvent( GameSettings settings ) {
		this.settings = settings;
	}

	/**
	 * @return the settings
	 */
	protected GameSettings getSettings() {
		return settings;
	}


}
