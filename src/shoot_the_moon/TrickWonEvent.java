package shoot_the_moon;


public class TrickWonEvent extends Event {
	private Position winningPlayer;

	private Team winningTeam;

	/**
	 * @return the winningPlayer
	 */
	protected Position getWinningPlayer() {
		return winningPlayer;
	}

	/**
	 * @return the winningTeam
	 */
	protected Team getWinningTeam() {
		return winningTeam;
	}

	public TrickWonEvent( Position winningPlayer, Team winningTeam ) {
		this.winningPlayer = winningPlayer;
		this.winningTeam = winningTeam;
	}

}
