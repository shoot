package shoot_the_moon;


import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class Game {

	private List<Observer> observers;

	private Map<Team, Integer> score;

	// matching arrays/lists
	// players[i] has positions[i] and hands.get(i)
	private Position[] positions;
	private Player[] players;
	private List<List<Card>> hands;

	private GameSettings settings;

	private int numPlayers;

	public Game( GameSettings settings, Player[] players ) {
		this.settings = settings;
		this.players = players;
		
		//initialize observer list to empty
		observers = ListFactory.makeList();
		
		numPlayers = players.length;

		// Check that there is a correct number of players
		if ( numPlayers % Team.numTeams != 0
				|| numPlayers / Team.numTeams != settings
						.getNumPlayersPerTeam() || numPlayers < 2 ) {
			throw new RuntimeException(
					"Incorrect number of Players given to Game constructor." );
		}

		score = new TreeMap<Team, Integer>();

		// create positions to represent all of the players
		positions = new Position[numPlayers];
		for ( int i = 0; i < numPlayers; i++ ) {
			Team team = Team.allTeams[i % Team.numTeams];
			positions[i] = new Position( i, numPlayers, team, players[i]
					.getName() );
		}

		// add every player as an observer so they will receive events
		for ( Player p : players ) {
			addObserver( p );
		}

		hands = ListFactory.makeList();
		
		// initialize every player's hands to an empty list
		for ( int i = 0; i < numPlayers; i++ ) {
			List<Card> h = ListFactory.makeList();
			hands.add( h );
		}

		// for every team, intialize the score to 0
		for ( Team t : Team.allTeams ) {
			score.put( t, new Integer( 0 ) );
		}
	}

	/**
	 * Registers an observer to receive all Events generated by this Game
	 * 
	 * @param observer
	 *            the observer to register
	 */
	public void addObserver( Observer observer ) {
		// if the observer is not registered yet, add it to
		// our list of observers
		if ( !observers.contains( observer ) ) {
			observers.add( observer );
		}
	}

	public void playGame() {

		/*
		 * GAME STARTS
		 */

		// signal that the game has started
		sendEvent( new GameStartEvent( settings ) );

		// assign each player their position
		for ( int i = 0; i < numPlayers; i++ ) {
			players[i].acceptPosition( positions[i] );
		}

		// set inside the game loop before the loop is broken to keep track of the team
		// who wins the game
		Team gameWinner = null;
		boolean isATie = false;
		List<Team> tiedWinners = ListFactory.makeList();

		// dealerIndex keeps track of who the dealer is of every round
		// it is initialized to the first player
		int dealerIndex = 0;

		// loop until the round is over
		// this loop is broken from in the SCORING phase
		while ( true ) {

			sendEvent( new RoundStartEvent( positions[dealerIndex] ) );

			/*
			 * DEALING phase
			 */

			// create a deck
			List<Card> deck = settings.makeDeck();
			// shuffle the deck
			Collections.shuffle( deck );

			// deal the cards out to the players
			while ( !deck.isEmpty() ) {
				for ( int i = 0; i < numPlayers; i++ ) {
					Card cardDealt = deck.get( 0 );
					deck.remove( 0 );
					hands.get( i ).add( cardDealt );
					players[i].acceptCard( cardDealt );
					sendEvent( new CardDealtEvent( positions[i] ) );
				}
			}

			/*
			 * BIDDING phase
			 */
			// player left of the dealer is the first bidder
			int firstBidIndex = ( dealerIndex + 1 ) % numPlayers;

			Bid highestBid = null;
			Position highestBidder = null;

			// every player makes one bid from left of the dealer --> dealer
			for ( int i = 0; i < numPlayers; i++ ) {
				int bidIndex = ( firstBidIndex + i ) % numPlayers;
				Bid newBid = players[bidIndex]
						.decideBid( highestBid );

				// check to make sure bid is legal
				if ( highestBid == null ) {
					if ( !Rules.isBidLegal( settings, newBid ) ) {
						throw new RuntimeException( "Player made illegal bid." );
					}
				} else {
					if ( !Rules.isBidLegal( settings, newBid, highestBid ) ) {
						throw new RuntimeException( "Player made illegal bid." );
					}
				}
				// if the new bid is higher than the last bid, then it is the
				// current highest bid
				if ( highestBid == null || newBid.isBetterThan( highestBid ) ) {
					highestBid = newBid;
					highestBidder = positions[bidIndex];
				}

				// send a new BidMadeEvent
				sendEvent( new BidMadeEvent( newBid, positions[bidIndex] ) );
			}

			if ( highestBid == null ) {
				Map<Team,Integer> noTricksTaken = new TreeMap<Team,Integer>();
				for ( Team t: Team.allTeams ) {
					noTricksTaken.put( t, new Integer(0) );
				}
				//no one bid, round is over
				sendEvent( new RoundOverEvent( false, score, Bid.makePassBid(), noTricksTaken, positions[numPlayers-1] ) );
				//skip to end of loop and start a new round
				continue;
			}

			Bid contract = highestBid;
			Team contractTeam = highestBidder.getTeam();
			Trump trump = contract.getTrump();

			sendEvent( new ContractMadeEvent( contract, highestBidder ) );

			// Set up an array of booleans to determine whether or not each
			// player will play cards
			// this hand (ie. in case a team shoots, some players may not play
			// cards)
			boolean[] playsThisHand = new boolean[numPlayers];

			// for every player...
			for ( int i = 0; i < numPlayers; i++ ) {
				// if it's not a shoot, the team is on the opposite team that
				// made the contract, or the player was the player who made the
				// bid, this player will play
				if ( !contract.isShoot()
						|| positions[i].getTeam() != contractTeam
						|| positions[i] == highestBidder ) {
					playsThisHand[i] = true;
					// otherwise he will not
				} else {
					playsThisHand[i] = false;
				}
			}

			// TODO: Card exchange between team mates in a shoot scenario
			if ( contract.isShoot() ) {

			}

			/*
			 * TRICKS phase
			 */
			// leadPosition keeps track of which player contributes the first
			// card to a trick
			// it is initially the player who made the winning bid in the
			// bidding phase
			int leadPosition = highestBidder.getPosition();
			// keeps track of how many tricks each team has taken
			Map<Team, Integer> tricksTaken = new TreeMap<Team, Integer>();

			// initialize the number of tricks taken for all teams to 0
			for ( Team t : Team.allTeams ) {
				tricksTaken.put( t, new Integer( 0 ) );
			}

			// there will be a trick for every card in the players hand
			for ( int trickNumber = 0; trickNumber < settings.getDeckSize() / numPlayers; trickNumber++ ) {

				// trick[i] is the card played in this trick by players[i] at
				// positions[i]
				Card[] trick = new Card[numPlayers];

				// get lead card
				Card leadCard = players[leadPosition].playCard();
				// check to make sure the player actually has this card
				if ( !hands.get(leadPosition).contains( leadCard ) ) {
					throw new RuntimeException(
							"Player played card not in their hand." );
				}
				// remove the card from the lead player's hand
				hands.get(leadPosition).remove( leadCard );
				// add the card to the trick
				trick[leadPosition] = leadCard;

				// send an event showing that a card has been played
				sendEvent( new CardPlayedEvent( positions[leadPosition],
						leadCard ) );

				// get the rest of the cards in this trick from the remaining
				// players
				for ( int i = 1; i < numPlayers; i++ ) {
					// check to make sure the player will play this hand
					if ( playsThisHand[i] == true ) {
						// playerIndex represents the index of the player that
						// will play next (starts at the player left of
						// the player that lead)
						int playerIndex = ( leadPosition + i ) % numPlayers;
						// get a card from the player
						Card cardPlayed = players[playerIndex].playCard();
						// check to make sure player actually has this card
						if ( !hands.get(playerIndex).contains( cardPlayed ) ) {
							throw new RuntimeException(
									"Player played card not in their hand." );
						}
						// check to make sure card is legal
						if ( !Rules.isCardLegal( hands.get(playerIndex),
								cardPlayed, leadCard, trump ) ) {
							throw new RuntimeException(
									"Player tried to play illegal card." );
						}
						// remove the card from the player's hand
						hands.get(playerIndex).remove( cardPlayed );
						// add the card to the trick
						trick[playerIndex] = cardPlayed;

						// send an event showing that a card has been played
						sendEvent( new CardPlayedEvent( positions[playerIndex],
								cardPlayed ) );
					}
				}

				// decide who won this trick
				int winningPlayerIndex = getTrickWinnerIndex( trick,
						leadPosition, trump );
				Position winningPosition = positions[winningPlayerIndex];
				Team winningTeam = winningPosition.getTeam();

				// send an event showing that the trick has been won
				sendEvent( new TrickWonEvent( winningPosition, winningTeam ) );

				// set the leadPosition for the next trick to be the winner of
				// this trick
				leadPosition = winningPlayerIndex;

				// increment the tricksTaken for the team who won this trick
				Integer newNumTricks = new Integer( tricksTaken.get(
						winningTeam ).intValue() + 1 );
				tricksTaken.put( winningTeam, newNumTricks );
			}

			/*
			 * SCORING phase
			 */
			int tricksNeeded;

			// if the contract was a normal bid (ie 6 hearts) then the tricks
			// needed to fufill the contract is the number of the bid
			if ( contract.isNormalBid() ) {
				tricksNeeded = contract.getNumber();
				// else the contract was a shoot, so the team needed to take
				// all the tricks
			} else {
				tricksNeeded = Rules.getHandSize( settings );
			}

			int tricksTakenByContractTeam = tricksTaken.get( contractTeam );

			// the contract was made if the tricks taken by the contract team
			// exceeded the number they needed
			boolean contractMade = tricksNeeded <= tricksTakenByContractTeam;

			if ( contract.isNormalBid() ) {
				// if the contract wasn't a shoot...
				if ( contractMade ) {
					// if the contract was made and the bid wasn't a shoot, each
					// team gets the number of tricks they took as points
					// as long as they are below the leech limit or they held
					// the contract
					for ( Team t : Team.allTeams ) {
						int t_score = score.get( t ).intValue();
						if ( t == contractTeam
								|| t_score < Rules.getLeechLimit( settings ) ) {
							// increase the team's score
							t_score += tricksTaken.get( t ).intValue();
							score.put( t, new Integer( t_score ) );
						}
					}
					// else the contract wasn't made...
				} else {
					// since the contract was made, the other team(s) get the
					// number of tricks the other team called, plus the tricks
					// that they took
					for ( Team t: Team.allTeams ) {
						if ( t != contractTeam ) {
							int t_score = score.get( t ).intValue();
							t_score += contract.getNumber();
							t_score += tricksTaken.get( t );
							//put the score back into the map...
							score.put( t, new Integer( t_score ) );
						}
					}
				}
			//else the contract was a shoot
			} else {
				if ( contractMade ) {
					//team that made the contract gets number of total tricks * 2 * shoot number
					//ie single shoot = 2*total tricks, double shoot = 4*total tricks, triple shoot = 6*total tricks
					int t_score = score.get( contractTeam );
					t_score += 2 * Rules.getHandSize( settings ) * contract.getShootNumber();
					
					//put score back into map
					score.put( contractTeam, new Integer( t_score ) );
				//else the team got euchred
				} else {
					//other teams get 2* total tricks*shoot number + tricks they took
					for ( Team t: Team.allTeams ) {
						if ( t!= contractTeam ) {
							int t_score = score.get( t );
							t_score += 2 * Rules.getHandSize( settings ) * contract.getShootNumber();
							t_score += tricksTaken.get( t );
							
							//put score back into map
							score.put( t, new Integer( t_score ) );
						}
					}
				}
			}
				
			// send an event saying that the round is over, whether or not
			// the contract was made, and the new current score
			sendEvent( new RoundOverEvent( contractMade, score, contract, tricksTaken, highestBidder ) );

			// if the score is or has exceeded the score needed to win, the game
			// is over
			boolean gameIsOver = false;
			isATie = false;
			gameWinner = null;
			
			for ( Team t: Team.allTeams ) {
				int t_score = score.get( t ).intValue();
				if ( t_score >= settings.getScoreNeededToWin() ) {
					if ( gameIsOver == false ) {
						gameIsOver = true;
						isATie = false;
						gameWinner = t;
					} else {
						int currentWinner_score = score.get( gameWinner ).intValue();
						if ( currentWinner_score < t_score ) {
							gameWinner = t;
							isATie = false;
						} else if ( currentWinner_score == t_score ) {
							isATie = true;
							gameWinner = null;
							//if there was already a tie, the game winner is already in the list
							//so we should add t to the tied list
							if ( tiedWinners.contains( gameWinner ) ) {
								tiedWinners.add( t );
							//else we clear the list and add the two teams
							} else {
								tiedWinners.clear();
								tiedWinners.add( gameWinner );
								tiedWinners.add( t );
							}
						}
					}
					gameIsOver = true;
				}
			}

			if ( gameIsOver ) {
				break;
			}

			/*
			 * Start another round
			 */
			dealerIndex = ( dealerIndex + 1 ) % numPlayers;

		} // loop back to the beginning of another round

		// send an event stating the game is over and the winning team(s)
		if ( isATie ) {
			sendEvent( new GameOverEvent( tiedWinners ) );
		} else {
			sendEvent( new GameOverEvent( gameWinner ) );
		}
	}

	/**
	 * Given an array of cards and a lead position, determines which card index
	 * should win the trick
	 * 
	 * @param trick
	 *            array of cards of the cards played (can contain null cards to
	 *            represent that a player did not play a card in case of a
	 *            shoot) the card at the lead position cannot be null
	 * @param leadPosition
	 *            the position of the first player to lead a card in the array
	 * @param trump
	 *            the trump suit
	 * @return the index of the card in trick that wins
	 */
	private int getTrickWinnerIndex( Card[] trick, int leadPosition, Trump trump ) {
		Card leadCard = trick[leadPosition];
		Card bestCard = leadCard;

		int bestCardIndex = leadPosition;

		for ( int i = 1; i < trick.length; i++ ) {
			int index = ( leadPosition + i ) % trick.length;
			Card c = trick[index];

			if ( c != null ) {
				if ( Rules.isCardBetter( bestCard, c, trump, leadCard
						.getContextualSuit( trump ) ) ) {
					bestCardIndex = index;
					bestCard = c;
				}
			}

		}

		return bestCardIndex;
	}

	/**
	 * Sends an event to all registered observers
	 * 
	 * @param e
	 *            the event to send
	 */
	private void sendEvent( Event e ) {
		for ( Observer o : observers ) {
			o.observeEvent( e );
		}
	}
}
