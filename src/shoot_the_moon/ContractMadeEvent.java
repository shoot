package shoot_the_moon;


public class ContractMadeEvent extends Event {
	private Bid winningBid;
	private Position winningPosition;

	public ContractMadeEvent( Bid winningBid, Position winningPosition ) {
		this.winningBid = winningBid;
		this.winningPosition = winningPosition;
	}

	/**
	 * @return the winningBid
	 */
	public Bid getWinningBid() {
		return winningBid;
	}

	/**
	 * @return the winningPlayer
	 */
	public Position getWinningPosition() {
		return winningPosition;
	}
	
	public Team getWinningTeam() {
		return winningPosition.getTeam();
	}

}
