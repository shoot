package shoot_the_moon;

/*
 * Represents the 4 states a Euchre card's suit can be
 * This can & should be treated like an enum:
 * ie. Suit s = Suit.SPADES;
 */
public final class Suit {

	private String name;

	private int id;

	private char character;

	private Suit( String name, int id, char character ) {
		this.name = name;
		this.id = id;
		this.character = character;

	}

	/*
	 * The following static Suits are the only 4 values a Suit variable can be
	 * initialized to since the Suit constructor is declared private and the
	 * class cannot be extended
	 */
	public static final Suit SPADES = new Suit( "Spades", 1, '\u2660' );

	public static final Suit HEARTS = new Suit( "Hearts", 2, '\u2665' );

	public static final Suit CLUBS = new Suit( "Clubs", 3, '\u2663' );

	public static final Suit DIAMONDS = new Suit( "Diamonds", 4, '\u2666' );

	/*
	 * allSuits is an array of the four suits, which can be used if you need to
	 * perform an operation for all 4 suits
	 */
	public static final Suit[] allSuits = { SPADES, HEARTS, CLUBS, DIAMONDS };

	/*
	 * Returns the unicode character that commonly represents this suit
	 */
	public char getChar() {
		return character;
	}

	/*
	 * Returns the written name of the this suit
	 */
	public String getName() {
		return name;
	}

	/*
	 * Get the opposite coloured suit ie. SPADES.getOppositeColour() = CLUBS
	 */
	public Suit getOppositeColour() {
		Suit opposite = null;

		if ( this == SPADES ) {
			opposite = CLUBS;
		} else if ( this == CLUBS ) {
			opposite = SPADES;
		} else if ( this == HEARTS ) {
			opposite = DIAMONDS;
		} else if ( this == DIAMONDS ) {
			opposite = HEARTS;
		}

		return opposite;
	}

	public boolean isBlack() {
		return ( this == Suit.CLUBS || this == Suit.SPADES );
	}

	public boolean isRed() {
		return !this.isBlack();
	}

	public String toString() {
		return name;
	}

	public int getId() {
		return id;
	}
}
