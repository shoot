package shoot_the_moon;

import shoot_the_moon.TextObserver;

public class ShootTheMoon {
    
    private static String _implVersion = ShootTheMoon.class.getPackage().getImplementationVersion();
    public static String VERSION = ( _implVersion == null ? "development" : _implVersion );
    
	/**
	 * @param args
	 */
	public static void main( String[] args ) {
		GameSettings settings = GameSettings.FOURPLAYER;
		Player[] players = new Player[4];
		
		players[0] = new DummyPlayer("Ben");
		players[1] = new DummyPlayer("Elizabeth");
		players[2] = new DummyPlayer("Darryl");
		players[3] = new DummyPlayer("Fraser");
		//players[4] = new DummyPlayer("Tim");
		//players[5] = new DummyPlayer("Tisi");
		
		Observer o = new TextObserver( System.out );
		
		Game g = new Game( settings, players );
		g.addObserver( o );
		
		g.playGame();

	}

}
