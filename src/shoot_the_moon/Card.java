package shoot_the_moon;

import java.util.List;


public final class Card {
	/*
	 * Private member data
	 */
	private Rank rank;

	private Suit suit;

	/*
	 * Default constructor
	 */
	Card( Rank rank, Suit suit ) throws IllegalArgumentException {
		if ( rank==null || suit == null ) {
			throw new IllegalArgumentException( "Null parameters in Card constructor" );
		}
		this.rank = rank;
		this.suit = suit;
	}

	/*
	 * Copy constructor
	 */
	public Card( Card card ) {
		this.rank = card.rank;
		this.suit = card.suit;
	}

	/*
	 * Gets the rank of the card using a cards.Card.Rank enum type
	 */
	public Rank getRank() {
		return rank;
	}

	/*
	 * Gets the rank of the card using a cards.Card.Suit enum type
	 */
	public Suit getSuit() {
		return suit;
	}

	public ContextualRank getContextualRank( Trump trump ) {
		// if trump is a suit and the card is a Jack, we need to check if it's a
		// left or a right
		if ( rank == Rank.JACK && trump.isSuit() ) {
			Suit trumpSuit = trump.getSuit();

			if ( suit == trumpSuit ) {
				return ContextualRank.RIGHT;
			} else if ( suit == trumpSuit.getOppositeColour() ) {
				return ContextualRank.LEFT;
			} else {
				return ContextualRank.JACK;
			}
			// for all other cards we can just return the same rank as the
			// original card
		} else {
			return rank.getContextualRank();
		}
	}

	public Suit getContextualSuit( Trump trump ) {
		// if trump is a suit and the card is a Jack, we need to check if it's a
		// left or a right
		if ( rank == Rank.JACK && trump.isSuit() ) {
			// get a Suit enum type from our trump enum
			Suit trumpSuit = trump.getSuit();

			if ( suit == trumpSuit ) {
				return trumpSuit;
			} else if ( suit == trumpSuit.getOppositeColour() ) {
				return trumpSuit;
			} else {
				return suit;
			}
			// for all other cards we can just set the rank and suit the same as
			// the actual card
		} else {
			return suit;
		}
	}

	/*
	 * Equality test
	 */
	public boolean equals( Card c ) {
		return ( this.suit == c.suit && this.rank == c.rank );
	}
	
	/*
	 * Get card short name
	 */
	public String getShortString() {
		return this.rank.getShortName() + this.suit.getChar();
	}
	
	public String toString() {
		return getShortString();
	}
	
	/*
	 * Return a new List<Card> containing all of the cards of a contextual suit
	 * given a list of cards and a particular trump context
	 */
	public static List<Card> getCardsOfContextualSuit( List<Card> cardList, Suit suit, Trump trump ) {
		List<Card> returnList = ListFactory.makeList();

		for ( Card c : cardList ) {
			if ( c.getContextualSuit( trump ) == suit ) {
				returnList.add( c );
			}
		}

		return returnList;
	}
	
	/*
	 * Return a new List<Card> containing all of the cards of a suit
	 * given a list of cards and a particular trump context
	 */
	public static List<Card> getCardsOfContextualSuit( List<Card> cardList, Suit suit ) {
		List<Card> returnList = ListFactory.makeList();

		for ( Card c : cardList ) {
			if ( c.getSuit() == suit ) {
				returnList.add( c );
			}
		}

		return returnList;
	}
}
