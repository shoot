package shoot_the_moon;

import java.util.ArrayList;
import java.util.List;

public final class ListFactory {

	public final static <E> List<E> makeList() {
		List<E> newList = new ArrayList<E>();
		return newList;
	}
	
	public final static <E> List<E> makeList( List<E> list ) {
		List<E> newList = new ArrayList<E>( list );
		return newList;
	}
}
